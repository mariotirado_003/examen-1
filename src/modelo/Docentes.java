package modelo;

public class Docentes {
    private int numDocente,nivel,horasImpartidas;
    private String nombre,domicilio;
    private float pagoBasephi;

    public Docentes(){
        this.numDocente=numDocente;
        this.nivel=nivel;
        this.horasImpartidas=horasImpartidas;
        this.nombre=nombre;
        this.domicilio=domicilio;
        this.pagoBasephi=pagoBasephi;
        
    }
    
    public Docentes(int numDocente, int nivel, int horasImpartidas, String domicilio, float pagoBasephi) {
        this.numDocente = 0;
        this.nivel = 0;
        this.horasImpartidas = 0;
        this.nombre = "";
        this.domicilio = "";
        this.pagoBasephi = 0.0f;
    }
    public Docentes(Docentes obj){
        this.numDocente=obj.numDocente;
        this.nivel=obj.nivel;
        this.horasImpartidas=obj.horasImpartidas;
        this.nombre=obj.nombre;
        this.domicilio=obj.domicilio;
        this.pagoBasephi=obj.pagoBasephi;
        
    }

    public int getNumDocente() {
        return numDocente;
    }

    public void setNumDocente(int numDocente) {
        this.numDocente = numDocente;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public int getHorasImpartidas() {
        return horasImpartidas;
    }

    public void setHorasImpartidas(int horasImpartidas) {
        this.horasImpartidas = horasImpartidas;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public float getPagoBasephi() {
        return pagoBasephi;
    }

    public void setPagoBasephi(float pagoBasephi) {
        this.pagoBasephi = pagoBasephi;
    }
    
    public double calcularPago(){
        double pagoTotal = 0;
        if (nivel == 1) {
            pagoTotal = pagoBasephi * horasImpartidas * 1.3;
        } else if (nivel == 2) {
            pagoTotal = pagoBasephi * horasImpartidas * 1.5;
        } else if (nivel == 3) {
            pagoTotal = pagoBasephi * horasImpartidas * 2.0;
        }
        return pagoTotal;
    }
    
    public double calcularImpuesto(double pagoTotal){
        double impuesto = pagoTotal * 0.16;
        return impuesto;
    }
    
    public double calcularBono(int cantidadHijos){
        double bono = 0.0;
        
        if (cantidadHijos >= 1 && cantidadHijos <= 2) {
            bono = calcularPago() * 0.05;
        } else if (cantidadHijos >= 3 && cantidadHijos <= 5) {
            bono = calcularPago() * 0.10;
        } else if (cantidadHijos > 5) {
            bono = calcularPago() * 0.20;
        }
        
        return bono;
    }
    
}
